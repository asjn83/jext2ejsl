package de.thm.icampus.mdd.model.sql

/**
 * Created by alexheinz1110 on 14.08.15.
 */
case class Reference(attribute: String, entity: String, reference: String, lower: Int, upper: Int) {}
