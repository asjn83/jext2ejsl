package de.thm.icampus.mdd.model.sql

/**
 * Created by alexheinz1110 on 14.08.15.
 */
case class Attribute(name: String, dbtype: String, htmltyp: String, isprimary: Boolean = false) {}
